$Env:CGO_ENABLED = 0

go run main.go  --sqltype=mysql `
    --connstr="root:daigou123@tcp(119.23.138.52:3306)/daigou" `
    --database=daigou `
    --json `
    --json-fmt=snake `
    --gorm `
    --model=entity `
    --out=./app/model `
    --overwrite `
    --guregu `
    --verbose `
    --templateDir=./template `
    --mapping=./template/mapping.json `
    --generate-dao `
    --verbose `
    # --model_naming="{{FmtFieldName . }}" `
    # --field_naming= "{{FmtFieldName (stringifyFirstChar . )}} "`
    # --file_naming= "{{ . }}" `
    # --name_test=orders_goods