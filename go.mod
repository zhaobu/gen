module gitee.com/zhaobu/gen

go 1.14

require (
	github.com/bxcodec/faker/v3 v3.4.0
	github.com/davecgh/go-spew v1.1.1
	github.com/denisenkom/go-mssqldb v0.0.0-20200428022330-06a60b6afbbc
	github.com/droundy/goopt v0.0.0-20170604162106-0b8effe182da
	github.com/gobuffalo/packd v1.0.0
	github.com/gobuffalo/packr/v2 v2.8.0
	github.com/iancoleman/strcase v0.0.0-20191112232945-16388991a334
	github.com/jimsmart/schema v0.0.4
	github.com/jinzhu/gorm v1.9.13
	github.com/jinzhu/inflection v1.0.0
	github.com/lib/pq v1.7.0
	github.com/mattn/go-sqlite3 v2.0.3+incompatible
	github.com/ompluscator/dynamic-struct v1.2.0
	github.com/serenize/snaker v0.0.0-20171204205717-a683aaf2d516
)
